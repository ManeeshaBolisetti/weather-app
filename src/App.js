import React from 'react';
import {useState,useEffect,useRef} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Table from 'react-bootstrap/Table';
import axios from 'axios';
import moment from 'moment';
import {Form,Row,Col,Button,InputGroup} from 'react-bootstrap'
import {AiFillQuestionCircle} from 'react-icons/ai';
function App() {
  const [wetherdata,setWetherData]=useState([]);
  const [name,setName]=useState('India');
  const [load,setload]=useState(false);
    const formRef = useRef();
  const cNameRef = useRef();
   useEffect(()=>{
 axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${name}&appid=1635890035cbba097fd5c26c8ea672a1`)
 .then((response) => {
     
 console.log(response);
      setload(false);
      setWetherData(response.data)

     })
    
   },[name])
  console.log(wetherdata.list)
  const submitHandler = (event) => {
    event.preventDefault();
    setload(true);
    setName(cNameRef.current.value);

  }
    
    
   
  return (
    <div className="App">
    <div className='main'> <h1 style={{color:'#ec6536', fontfamily:'sans-serif'}}>Weather In Your City</h1>
     
   <Form onSubmit={submitHandler} ref={formRef} className='main'>  
          <Form.Control 
            type='text'
            placeholder='Search'
            style={{border:'1px solid #ec6536',margin:'5px'}}
            ref={cNameRef}
          /> 
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Button  style={{backgroundColor:'#ec6536' ,border:'none',margin:'5px'}}
              type='submit'
            ><AiFillQuestionCircle/>Search</Button>
             {/* </InputGroup> */}
</Form>
{load && <div class="loader"></div>}

    </div>
     <div class="container">
     {load && <div >No Data Is found</div>}
     <br/>
 {!load && 

  <div class="row">

  {wetherdata?.list?.map((we)=>(
  
  
moment(we.dt_txt).format('LTS') === '12:00:00 AM' && 
  <div class="col-sm">
 
 
< table class="table table-bordered" style={{border:'1px solid' ,width:'100%'}}>
  <thead>
    <tr>
      <th colspan="4" style={{backgroundColor: "#ec6536"}}> Date:{ moment(we.dt_txt).format('L')}</th>
    </tr>
  </thead>
  <tbody>
    <tr style={{backgroundColor: "#cccccc"}}>
      <th colspan="4">Temprature</th>
      
    </tr>
    <tr style={{backgroundColor: "#cccccc"}}>
      <th colspan="2">Min</th>
      <th colspan="2">Max</th>

    </tr>
    <tr style={{backgroundColor: "#cccccc"}}>
      <td colspan="2">{we.main.temp_max}</td>
      <td colspan="2">{we.main.temp_max}</td>
     
    </tr>
    <tr>
      <th colspan="2">Pressure</th>
      <td colspan="2">{we.main.pressure}</td>

    </tr>
    <tr>
      <th colspan="2">Humadity</th>
      <td colspan="2">{we.main.humidity}</td>
     
    </tr>
  </tbody>
</table>
 

    </div>

  ))}
        </div>
        } 
</div>


    </div>
  );
}

export default App;
